<?php

if (
    empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW'])
) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="web_6"');
    print('<h1>Нет доступа</h1>');
    exit();
}
$user = 'u35649';
$pass = '35734534';
$db = new PDO('mysql:host=localhost;dbname=u35649', $user, $pass, array(PDO::ATTR_PERSISTENT => true));

$stmt = $db->prepare("SELECT * FROM adminData");
$stmt->execute();
$admidData = $stmt->fetch(PDO::FETCH_ASSOC);
$flag = 0;
if (($admidData['adminId'] == $_SERVER['PHP_AUTH_USER'] && $admidData['adminPass'] == $_SERVER['PHP_AUTH_PW'])) {
    $flag = 1;
}

if ($flag == 0) {
    header('HTTP/1.1 401 Unanthorized');
    header('WWW-Authenticate: Basic realm="web_6"');
    print('<h1>401 Нет доступа<</h1>');
    exit();
}
print('Панель администратора');
?>
<html>

<head>
    <meta charset="utf-8" />
    <title>Панель администратора</title>
    <link rel="stylesheet" href="../styles/admin.css" />

</head>

<body>
    <form action="delete.php" method="POST" accept-charset="UTF-8">
        <label>
            <input type="number" name="delete">
        </label>
        <input type="submit" style="margin-bottom : -1em" class="buttonform" value="Удалить запись по ID">
    </form>
    <form action="change.php" method="POST" accept-charset="UTF-8">
        <label>
            <input type="number" name="change">
        </label>
        <input type="submit" style="margin-bottom : -1em" class="buttonform" value="Изменить запись по ID">
    </form>
    <?php

    $request = "SELECT * from user JOIN userPower on user.userId = userPower.userId
join loginData on loginData.userId = user.userId ;";
    $result = $db->prepare($request);
    $result->execute();
    print '<table class="table">';
    print '<tr><th>ID</th><th>Имя</th><th>E-Mail</th><th>Дата рождения</th><th>Пол</th><th>Кол-во конечностей</th>
    <th>Биография</th><th>Логин</th><th>Хэш пароля</th><th>Способность</th></tr>';
    while ($data = $result->fetch(PDO::FETCH_ASSOC)) {
        print '<tr><td>';
        print $data['userId'];
        print '</td><td>';
        print $data['name'];
        print '</td><td>';
        print $data['email'];
        print '</td><td>';
        print $data['date'];
        print '</td><td>';
        print $data['gender'];
        print '</td><td>';
        print $data['amountOfColumn'];
        print '</td><td>';
        print $data['biography'];
        print '</td><td>';
        print $data['loginId'];
        print '</td><td>';
        print $data['passId'];
        print '</td><td>';
        print $data['powerId'];
        print '</td></tr>';
    }
    print '</table>';
    $powersStatistic = array();
    $request = "SELECT COUNT(powerId) FROM userPower where powerId='1' group by powerId ;";
    $result = $db->prepare($request);
    $result->execute();
    $powersStatistic[0] = $result->fetch()[0];
    $request = "SELECT COUNT(powerId) FROM userPower where powerId='2' group by powerId ;";
    $result = $db->prepare($request);
    $result->execute();
    $powersStatistic[1] = $result->fetch()[0];
    $request = "SELECT COUNT(powerId) FROM userPower where powerId='3' group by powerId ;";
    $result = $db->prepare($request);
    $result->execute();
    $powersStatistic[2] = $result->fetch()[0];

    print '<h2>Статистика по сверхспособностям:</h2>';
    print '<table class="table">';
    print '<tr><th>Скорочтение</th><th>Хорошая физическая подготовка</th><th>Фотографическая память</th></tr>';
    print '<tr><td>';
    print $powersStatistic[0];
    print '</td><td>';
    print $powersStatistic[1];
    print '</td><td>';
    print $powersStatistic[2];
    print '</td>';
    print '</tr>';
    print '</table>';
    ?>
</body>

</html>